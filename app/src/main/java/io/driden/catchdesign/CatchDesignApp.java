package io.driden.catchdesign;

import android.app.Application;
import android.content.Context;

import io.driden.catchdesign.injection.component.AppComponent;
import io.driden.catchdesign.injection.component.DaggerAppComponent;
import io.driden.catchdesign.injection.module.AppModule;
import io.driden.catchdesign.injection.module.NetworkModule;
import timber.log.Timber;

public class CatchDesignApp extends Application {

    private AppComponent appComponent;

    public static CatchDesignApp get(Context context) {
        return (CatchDesignApp) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    public AppComponent getComponent() {
        if (appComponent == null) {
            appComponent = DaggerAppComponent.builder()
                    .networkModule(new NetworkModule(this))
                    .appModule(new AppModule(this))
                    .build();
        }
        return appComponent;
    }

    // Needed to replace the component with a test specific one
    public void setComponent(AppComponent appComponent) {
        this.appComponent = appComponent;
    }
}
