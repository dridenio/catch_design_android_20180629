package io.driden.catchdesign.features.detail;

import io.driden.catchdesign.features.base.MvpView;

public interface DetailMvpView extends MvpView {
    void displayContent(String content);
}
