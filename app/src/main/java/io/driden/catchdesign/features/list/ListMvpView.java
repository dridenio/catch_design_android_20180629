package io.driden.catchdesign.features.list;

import java.util.List;

import io.driden.catchdesign.data.model.response.DataPojo;
import io.driden.catchdesign.features.base.MvpView;

public interface ListMvpView extends MvpView {

    void showList(List<DataPojo> dataPojos);

    void showProgress(boolean b);
}
