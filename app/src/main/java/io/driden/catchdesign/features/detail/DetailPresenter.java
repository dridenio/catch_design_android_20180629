package io.driden.catchdesign.features.detail;

import javax.inject.Inject;

import io.driden.catchdesign.features.base.BasePresenter;
import io.driden.catchdesign.injection.ConfigPersistent;

@ConfigPersistent
public class DetailPresenter extends BasePresenter<DetailMvpView> {

    @Inject
    public DetailPresenter(){

    }

    @Override
    public void attachView(DetailMvpView mvpView) {
        super.attachView(mvpView);
    }

    public void setContent(String content) {
        getView().displayContent(content);
    }

}
