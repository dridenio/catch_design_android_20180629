package io.driden.catchdesign.features.detail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import io.driden.catchdesign.R;
import io.driden.catchdesign.features.base.BaseActivity;
import io.driden.catchdesign.injection.component.ActivityComponent;

public class DetailActivity extends BaseActivity implements DetailMvpView {

    public static String EXTRA_CONTENT = "content";

    @BindView(R.id.tvContent)
    TextView tvContent;

    @Inject
    DetailPresenter mPresenter;

    private String content;

    public static Intent getStartIntent(Context context, String content) {
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra(EXTRA_CONTENT, content);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        content = getIntent().getStringExtra(EXTRA_CONTENT);
        if (content == null) {
            throw new IllegalArgumentException("Content is null");
        }

        mPresenter.setContent(content);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_detail;
    }

    @Override
    protected void inject(ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }

    @Override
    protected void attachView() {
        mPresenter.attachView(this);
    }

    @Override
    protected void detachPresenter() {
        mPresenter.detachView();
    }


    @Override
    public void displayContent(String content) {
        tvContent.setText(content);
    }
}
