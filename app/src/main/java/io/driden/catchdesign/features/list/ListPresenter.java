package io.driden.catchdesign.features.list;

import javax.inject.Inject;

import io.driden.catchdesign.data.DataManager;
import io.driden.catchdesign.features.base.BasePresenter;
import io.driden.catchdesign.injection.ConfigPersistent;
import io.driden.catchdesign.util.rx.scheduler.SchedulerUtils;

@ConfigPersistent
public class ListPresenter extends BasePresenter<ListMvpView> {

    private final DataManager dataManager;

    @Inject
    public ListPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Override
    public void attachView(ListMvpView mvpView) {
        super.attachView(mvpView);
    }

    public void getList() {
        checkViewAttached();
        getView().showProgress(true);
        dataManager.getDataList()
                .compose(SchedulerUtils.ioToMain())
                .subscribe(
                        dataPojos -> {
                            getView().showProgress(false);
                            getView().showList(dataPojos);
                        },
                        throwable -> {
                            getView().showProgress(false);
                        });
    }
}
