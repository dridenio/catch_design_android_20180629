package io.driden.catchdesign.features.list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.driden.catchdesign.R;
import io.driden.catchdesign.data.model.response.DataPojo;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

public class DataListAdapter extends RecyclerView.Adapter<DataListAdapter.DataViewHolder> {

    private List<DataPojo> dataList;
    private Subject<DataPojo> selectedData;

    @Inject
    DataListAdapter() {
        dataList = Collections.emptyList();
        selectedData = PublishSubject.create();
    }

    public void setDataPojos(List<DataPojo> dataList) {
        this.dataList = dataList;
        notifyDataSetChanged();
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view =
                LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_data, parent, false);
        return new DataViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {
        DataPojo data = this.dataList.get(position);
        holder.onBind(data);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    Observable<DataPojo> getClickedItem() {
        return selectedData;
    }

    class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvSubTitle)
        TextView tvSubTitle;

        private DataPojo data;

        DataViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> selectedData.onNext(data));
        }

        void onBind(DataPojo data) {
            this.data = data;
            tvTitle.setText(data.title);
            tvSubTitle.setText(data.subtitle);
        }
    }
}
