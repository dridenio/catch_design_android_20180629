package io.driden.catchdesign.features.list;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import io.driden.catchdesign.R;
import io.driden.catchdesign.data.model.response.DataPojo;
import io.driden.catchdesign.features.base.BaseActivity;
import io.driden.catchdesign.features.detail.DetailActivity;
import io.driden.catchdesign.injection.component.ActivityComponent;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

public class ListActivity extends BaseActivity implements ListMvpView {

    @Inject
    DataListAdapter dataListAdapter;
    @Inject
    ListPresenter mPresenter;

    @BindView(R.id.rvList)
    RecyclerView rvList;

    @BindView(R.id.ivBg)
    ImageView ivBg;

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        swipe.setProgressBackgroundColorSchemeResource(R.color.primary);
        swipe.setColorSchemeResources(R.color.white);
        swipe.setOnRefreshListener(() ->  mPresenter.getList());

        rvList.setLayoutManager(new LinearLayoutManager(this));
        rvList.setAdapter(dataListAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this,
                LinearLayoutManager.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider_custom));

        rvList.addItemDecoration(dividerItemDecoration);
        setClickObserver();
    }

    private void setClickObserver() {
        Disposable disposable =
                dataListAdapter.getClickedItem()
                        .subscribe(
                                dataPojo ->
                                        startActivity(DetailActivity.getStartIntent(this, dataPojo.content)),
                                throwable -> Timber.e(throwable, "Data click is failed."));
        mPresenter.addDisposable(disposable);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_list;
    }

    @Override
    protected void inject(ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }

    @Override
    protected void attachView() {
        mPresenter.attachView(this);
    }

    @Override
    protected void detachPresenter() {
        mPresenter.detachView();
    }

    @Override
    public void showList(List<DataPojo> dataPojos) {
        dataListAdapter.setDataPojos(dataPojos);
        if(dataPojos != null && dataPojos.size() > 0) {
            rvList.setVisibility(View.VISIBLE);
            ivBg.setVisibility(View.GONE);
        }else{
            rvList.setVisibility(View.GONE);
            ivBg.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showProgress(boolean b) {
        swipe.setRefreshing(b);
    }
}
