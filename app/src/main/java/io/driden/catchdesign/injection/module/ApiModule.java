package io.driden.catchdesign.injection.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.driden.catchdesign.data.remote.CatchDataService;
import retrofit2.Retrofit;

@Module(includes = {NetworkModule.class})
public class ApiModule {

    @Provides
    @Singleton
    CatchDataService provideCatchAPI(Retrofit retrofit) {
        return retrofit.create(CatchDataService.class);
    }
}
