package io.driden.catchdesign.injection.component;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import io.driden.catchdesign.data.DataManager;
import io.driden.catchdesign.injection.ApplicationContext;
import io.driden.catchdesign.injection.module.AppModule;

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {

    @ApplicationContext
    Context context();

    Application application();

    DataManager apiManager();
}
