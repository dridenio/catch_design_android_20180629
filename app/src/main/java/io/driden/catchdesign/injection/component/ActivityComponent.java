package io.driden.catchdesign.injection.component;

import dagger.Subcomponent;
import io.driden.catchdesign.features.detail.DetailActivity;
import io.driden.catchdesign.features.list.ListActivity;
import io.driden.catchdesign.injection.PerActivity;
import io.driden.catchdesign.injection.module.ActivityModule;

@PerActivity
@Subcomponent(modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(ListActivity activity);
    void inject(DetailActivity activity);
}
