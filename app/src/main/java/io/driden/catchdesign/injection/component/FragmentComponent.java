package io.driden.catchdesign.injection.component;

import dagger.Subcomponent;
import io.driden.catchdesign.injection.PerFragment;
import io.driden.catchdesign.injection.module.FragmentModule;

/**
 * This component inject dependencies to all Fragments across the application
 */
@PerFragment
@Subcomponent(modules = FragmentModule.class)
public interface FragmentComponent {
}
