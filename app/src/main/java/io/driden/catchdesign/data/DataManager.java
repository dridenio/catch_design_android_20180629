package io.driden.catchdesign.data;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.driden.catchdesign.data.model.response.DataPojo;
import io.driden.catchdesign.data.remote.CatchDataService;
import io.reactivex.Single;

@Singleton
public class DataManager {

    private CatchDataService apiService;

    @Inject
    public DataManager(CatchDataService apiService) {
        this.apiService = apiService;
    }

    public Single<List<DataPojo>> getDataList() {
        return apiService.getJsonData();
    }
}
