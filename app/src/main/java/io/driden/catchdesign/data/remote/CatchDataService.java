package io.driden.catchdesign.data.remote;

import java.util.List;

import io.driden.catchdesign.data.model.response.DataPojo;
import io.reactivex.Single;
import retrofit2.http.GET;

public interface CatchDataService {

    @GET("catchnz/android-test/master/data/data.json")
    Single<List<DataPojo>> getJsonData();

}
