package io.driden.catchdesign.data.model.response;

public class DataPojo {
    public int id;
    public String title;
    public String subtitle;
    public String content;
}
