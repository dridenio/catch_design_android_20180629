package io.driden.catchdesign;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import io.driden.catchdesign.data.DataManager;
import io.driden.catchdesign.data.model.response.DataPojo;
import io.driden.catchdesign.features.list.ListMvpView;
import io.driden.catchdesign.features.list.ListPresenter;
import io.driden.catchdesign.util.RxSchedulersOverrideRule;
import io.reactivex.Single;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ListPresenterTest {

    @Rule
    public final RxSchedulersOverrideRule overrideSchedulersRule = new RxSchedulersOverrideRule();

    @Mock
    ListMvpView mockListMvpView;
    @Mock
    DataManager mockDataManager;
    private ListPresenter liistPresenter;

    @Before
    public void setUp() {
        liistPresenter = new ListPresenter(mockDataManager);
        liistPresenter.attachView(mockListMvpView);
    }

    @After
    public void tearDown() {
        liistPresenter.detachView();
    }

    @Test
    public void getListReturnsDataList() throws Exception {
        List<DataPojo> dataPojos = getMockList(10);
        when(mockDataManager.getDataList()).thenReturn(Single.just(dataPojos));

        liistPresenter.getList();

        verify(mockListMvpView).showList(dataPojos);
    }

    private List<DataPojo> getMockList(int count){
        List<DataPojo> dataPojos = new ArrayList<>();
        for(int i = 0; i < 3; i++){
            DataPojo tempData = new DataPojo();
            tempData.id = i;
            tempData.title = String.valueOf(i);
            tempData.subtitle = String.valueOf(i);
            tempData.content = "whatever";
            dataPojos.add(tempData);
        }

        return dataPojos;
    }
}
