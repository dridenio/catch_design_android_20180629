package io.driden.catchdesign;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import io.driden.catchdesign.data.model.response.DataPojo;
import io.driden.catchdesign.features.detail.DetailMvpView;
import io.driden.catchdesign.features.detail.DetailPresenter;
import io.driden.catchdesign.util.RxSchedulersOverrideRule;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class DetailPresenterTest {

    @Rule
    public final RxSchedulersOverrideRule overrideSchedulersRule = new RxSchedulersOverrideRule();

    @Mock
    DetailMvpView mockDetailMvpView;
    private DetailPresenter detailPresenter;

    @Before
    public void setUp() {
        detailPresenter = new DetailPresenter();
        detailPresenter.attachView(mockDetailMvpView);
    }

    @After
    public void tearDown() {
        detailPresenter.detachView();
    }

    @Test
    public void getDataPojoReturnContent() throws Exception {
        DataPojo dataPojo = new DataPojo();
        dataPojo.id = 3;
        dataPojo.title = "test title";
        dataPojo.subtitle = "test subtitle";
        dataPojo.content = "whatever";

        detailPresenter.setContent(dataPojo.content);

        verify(mockDetailMvpView).displayContent(dataPojo.content);
    }
}
